package model;

import exceptions.DuplicateModelNameException;
import exceptions.ModelPriceOutOfBoundsException;
import exceptions.NoSuchModelNameException;
import model.interfaces.Transport;

import java.util.Arrays;

public class Auto implements Transport {
    private String mark;
    private Model[] models;

    public Auto(String mark, int modelsLength){
        setMark(mark);
        models = new Model[modelsLength];
        for (int i = 0; i < models.length; i++){
            models[i] = new Model();
        }
    }

    public String[] getAllModelNames(){
        String[] modelNames = new String[models.length];
        for (int i = 0; i < models.length; i++){
            modelNames[i] = models[i].getModelName();
        }
        return modelNames;
    }

    public void renameModel(String oldModelName, String newModelName) throws DuplicateModelNameException, NoSuchModelNameException {
        Model oldModel = null;
        for (Model model : models){
            if (model.modelName.equals(oldModelName)){
                oldModel = model;
                break;
            }
        }
        if (oldModel != null){
            for (Model model : models){
                if (model.modelName.equals(newModelName)){
                    throw new DuplicateModelNameException(this.mark, newModelName);
                }
            }
            oldModel.modelName = newModelName;
        }
        else throw new NoSuchModelNameException(this.mark, newModelName);
    }

    public double getModelPrice(String modelName) throws NoSuchModelNameException {
        for (Model model : models) {
            if (model.getModelName().equals(modelName)) {
                return model.getModelPrice();
            }
        }
        throw new NoSuchModelNameException(this.mark, modelName);
    }

    public void setNewPrice(String modelName, double newPrice) throws NoSuchModelNameException {
        boolean modelNotFound = true;
        for (Model model : models){
            if (model.modelName.equals(modelName)){
                model.setModelPrice(newPrice);
                modelNotFound = false;
                break;
            }
        }
        if (modelNotFound) throw new NoSuchModelNameException(this.mark, modelName);
    }

    public double[] getAllModelsPrices(){
        double[] modelsPrices = new double[models.length];
        for (int i = 0; i < modelsPrices.length; i++){
            modelsPrices[i] = models[i].getModelPrice();
        }
        return modelsPrices;
    }

    public void addNewModel(String modelName, double modelPrice) throws DuplicateModelNameException {
        for (Model model : models){
            if (model.modelName.equals(modelName)) throw new DuplicateModelNameException(this.mark, modelName);
        }
        models = Arrays.copyOf(models, models.length+1);
        models[models.length -1] = new Model(modelName, modelPrice);
    }

    public void removeModel(String modelName, double modelPrice) throws NoSuchModelNameException {
        Model[] newModelsArray = null;

        for (int i = 0; i < models.length; i++){
            if (models[i].getModelName().equals(modelName) && models[i].getModelPrice() == modelPrice){
                newModelsArray = Arrays.copyOf(models, models.length - 1);
                System.arraycopy(models, i+1, newModelsArray, i, models.length - i -1);
            }
        }

        if (newModelsArray==null) throw new NoSuchModelNameException(this.mark, modelName);

        models = newModelsArray;
    }

    public int getModelsLength(){
        return models.length;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    class Model{
        String modelName;
        double modelPrice;

        public String getModelName() {
            return modelName;
        }

        public double getModelPrice() {
            return modelPrice;
        }

        public void setModelName(String modelName) {
            this.modelName = modelName;
        }

        public void setModelPrice(double modelPrice) {
            if (modelPrice < 0) throw new ModelPriceOutOfBoundsException(modelPrice);
            this.modelPrice = modelPrice;
        }

        public Model(){
            this.modelName = "Default name";
            this.modelPrice = 0;
        }

        public Model(String modelName, double price){
            setModelName(modelName);
            setModelPrice(price);
        }
    }
}
