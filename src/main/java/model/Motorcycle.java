package model;

import exceptions.DuplicateModelNameException;
import exceptions.ModelPriceOutOfBoundsException;
import exceptions.NoSuchModelNameException;
import model.interfaces.Transport;

public class Motorcycle implements Transport {
    private String mark;
    private Model head = new Model();
    private int size;

    {
        head.prev = head;
        head.next = head;
    }

    public Motorcycle(String mark, int size){
        this.mark = mark;
        this.size = size;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public void renameModel(String oldModel, String newModel) throws NoSuchModelNameException, DuplicateModelNameException {
        Model current = head.next;
        while(current!=head){
            if (!current.modelName.equals(oldModel)){
                current = current.next;
            }
            else break;
        }

        if (current == head) throw new NoSuchModelNameException(this.mark, newModel);

        Model node = head.next;
        while (node != head){
            if (!node.modelName.equals(newModel)) node = node.next;
            else throw new DuplicateModelNameException(this.mark, newModel);
        }

        current.modelName = newModel;
    }

    public String[] getAllModelNames(){
        String[] result = new String[this.size];
        Model current = head.next;
        int i = 0;
        while(current != head){
            result[i] = current.modelName;
            i++;
            current = current.next;
        }
        return result;
    }

    public double getModelPrice(String modelName) throws NoSuchModelNameException {
        Model current = head.next;
        while(current!=head){
            if (!current.modelName.equals(modelName)){
                current = current.next;
            }
            else break;
        }
        if (current == head) throw new NoSuchModelNameException(this.mark, modelName);
        return current.price;
    }

    public void setNewPrice(String modelName, double newPrice) throws NoSuchModelNameException {
        Model current = head.next;
        while (current != head){
            if (current.modelName.equals(modelName)){
                current.setPrice(newPrice);
                break;
            }
            else current = current.next;
        }
        if(current == head) throw new NoSuchModelNameException(this.mark, modelName);
    }

    public void addNewModel(String modelName, double price) throws DuplicateModelNameException {
        Model current = head.next;
        while (current!=head){
            if (!current.modelName.equals(modelName)){
                current = current.next;
            }
            else throw new DuplicateModelNameException(this.mark, modelName);
        }

        Model newModel = new Model(modelName, price);
        newModel.next = head;
        newModel.prev = head.prev;
        head.prev.next = newModel;
        head.prev = newModel;
        this.size++;
    }

    public void removeModel(String modelName, double price) throws NoSuchModelNameException {
        Model current = head.next;
        while(current!=head){
            if (current.modelName.equals(modelName)){
                Model prev = current.prev;
                Model next = current.next;
                prev.next = next;
                next.prev = prev;
                this.size--;
                break;
            }
            else current = current.next;
        }
        if (current == head) {
            throw new NoSuchModelNameException(this.mark, modelName);
        }
    }

    public int getModelsLength(){
        return this.size;
    }

    public double[] getAllModelsPrices() {
        double[] result = new double[this.size];
        Model current = head.next;
        int i = 0;
        while (current != head) {
            result[i] = current.price;
            i++;
            current = current.next;
        }
        return result;
    }

    private class Model{
        String modelName;
        double price;
        Model prev;
        Model next;

        Model(){
        }

        Model(String nameModel, double price){
            this.modelName = nameModel;
            this.price = price;
        }

        public String getModelName() {
            return modelName;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            if (price < 0) throw new ModelPriceOutOfBoundsException(price);
            this.price = price;
        }

        public void setModelName(String modelName) {
            this.modelName = modelName;
        }
    }
}
