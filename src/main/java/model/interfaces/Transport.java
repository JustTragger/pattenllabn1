package model.interfaces;

import exceptions.DuplicateModelNameException;
import exceptions.NoSuchModelNameException;

public interface Transport {
    String getMark();
    void setMark(String mark);
    void renameModel(String oldModel, String newModel)  throws DuplicateModelNameException, NoSuchModelNameException;
    double getModelPrice(String modelName) throws NoSuchModelNameException;
    String[] getAllModelNames();
    void setNewPrice(String modelName, double newPrice) throws NoSuchModelNameException;
    void addNewModel(String modelName, double modelPrice) throws DuplicateModelNameException;
    void removeModel(String modelName, double modelPrice) throws NoSuchModelNameException;
    int getModelsLength();
    double[] getAllModelsPrices();
}
