
import model.interfaces.Transport;

import java.util.Arrays;

public class TransportCoordinator {

    public static double countAverageModelsPrice(Transport transport){
        double averagePrice;
        double sum = 0;
        double[] modelsPrices = transport.getAllModelsPrices();
        for (double modelsPrice : modelsPrices) {
            sum += modelsPrice;
        }
        averagePrice = sum / modelsPrices.length;
        return averagePrice;
    }

    public static void showTransportInfo (Transport transport){
        System.out.println(Arrays.toString(transport.getAllModelNames()));
        System.out.println(Arrays.toString(transport.getAllModelsPrices()));
    }
}
