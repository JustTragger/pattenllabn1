package exceptions;

public class ModelPriceOutOfBoundsException extends RuntimeException{
    public ModelPriceOutOfBoundsException(double price){
        super("Incorrect price value: "+price+". Price should be more than 0");
    }
}
