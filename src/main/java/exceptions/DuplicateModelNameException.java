package exceptions;

public class DuplicateModelNameException extends Exception{
    public DuplicateModelNameException(String mark, String modelName){
        super("The model "+modelName+" already exist for mark "+mark);
    }
}
