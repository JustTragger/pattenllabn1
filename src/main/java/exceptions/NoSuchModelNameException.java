package exceptions;

public class NoSuchModelNameException extends Exception{
    public NoSuchModelNameException(String mark, String modelName){
        super("Model "+modelName+" not exist for this mark "+mark);
    }
}
