import exceptions.DuplicateModelNameException;
import model.Auto;

public class Main {
    public static void main(String[] args) throws DuplicateModelNameException {
        Auto auto = new Auto("Lada", 0);
        auto.addNewModel("Vesta", 758900);
        auto.addNewModel("Granta", 531900);
        auto.addNewModel("Xray", 730900);
    }
}
